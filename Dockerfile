FROM golang:1.11-alpine AS builder

ENV GO111MODULE=on

RUN apk update && apk add --no-cache git

WORKDIR $GOPATH/src/bitbucket.org/bonialgroup/monik8s/

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /go/bin/monik8s cmd/monik8s/main.go

FROM scratch

COPY --from=builder /go/bin/monik8s /monik8s

ENTRYPOINT ["/monik8s"]


