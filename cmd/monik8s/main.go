package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func recordRunningTime() {
	go func() {
		for {
			time.Sleep(time.Second)
			secondsRunning.Inc()
		}
	}()
}

var (
	secondsRunning = promauto.NewCounter(prometheus.CounterOpts{
		Name: "monik8s_running_seconds",
		Help: "The total number of seconds the app is running.",
	})

	delayHistogram = prometheus.NewHistogram(prometheus.HistogramOpts{
		Name:    "monik8s_delay_histogram_milliseconds",
		Help:    "Delay distributions.",
		Buckets: prometheus.ExponentialBuckets(2, 4, 20),
	})
)

const (
	defaultPort = "8080"
)

func init() {
	prometheus.MustRegister(delayHistogram)
}

func main() {
	recordRunningTime()

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	router := gin.Default()
	router.GET("/", defaultHandler)
	router.GET("/healthz", healthzHandler)
	router.GET("/404", notFoundErrorHandler)
	router.GET("/500", internalServerErrorHandler)
	router.GET("/delay", delayHandler)
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))

	log.Fatal(router.Run(":" + port))
}

func defaultHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"message": "yay!"})
}

func healthzHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "ok"})
}

func delayHandler(c *gin.Context) {
	delayMilliseconds, err := strconv.Atoi(c.Query("ms"))
	if err != nil || delayMilliseconds < 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": http.StatusText(http.StatusBadRequest)})
		return
	}

	time.Sleep(time.Duration(delayMilliseconds) * time.Millisecond)

	delayHistogram.Observe(float64(delayMilliseconds))

	c.JSON(http.StatusOK, gin.H{"message": "pew!"})
}

func notFoundErrorHandler(c *gin.Context) {
	c.JSON(http.StatusNotFound, gin.H{"error": http.StatusText(http.StatusNotFound)})
}

func internalServerErrorHandler(c *gin.Context) {
	c.JSON(http.StatusInternalServerError, gin.H{"error": http.StatusText(http.StatusInternalServerError)})
}
