module bitbucket.org/bonialgroup/monik8s

require (
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/prometheus/client_golang v0.9.2
	github.com/spf13/viper v1.3.2
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
)
